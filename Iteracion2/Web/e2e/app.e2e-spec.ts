import { AppPage } from './app.po';
import {by} from 'protractor'

describe('Hometap Web: Clientes', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  afterEach(() => {
    page.waitTest();
  });

  it('Se muestra la lista de clientes registrados en la aplicación', () => {
    page.navigateTo();
    page.login();
    page.goToClients();
    expect(page.getPageTitle()).toEqual('Clientes');
    page.waitTest();
    expect(page.getPageListCount()).toBeGreaterThan(1);
  });

  it('Se puede buscar un cliente', () => {
    // Por nombre
    page.goToClients();
    page.applySearchClients("Juan");
    expect(page.getListItemData(1, 1)).toContain("Juan");
    // Por telefono
    page.goToClients();
    page.applySearchClients("301");
    expect(page.getListItemData(1, 2)).toContain("301");
    // Por ID
    page.goToClients();
    page.applySearchClients("Ly4lCFJ67qbQx1MEvyKBzPSjiVf2");
    expect(page.getListItemData(1, 1)).toContain("Daniel Soto Rey");
  });

  it('Se pude navegar a un cliente', () => {
    page.goToClients();
    page.getPageListCount().then(function (number) {
      let userSelected = Math.floor(Math.random() * (number - 1)) + 1;
      let name = page.getListItemData(userSelected, 1);
      page.navigateToItem(userSelected);
      expect(page.getPageTitle()).toEqual(name);
    })
  });

  it('Se puede activar/desactivar un cliente', () => {
    page.goToClients();
    page.getPageListCount().then(function (number) {
      let userSelected = Math.floor(Math.random() * (number - 1)) + 1;
      let name = page.getListItemData(userSelected, 1);
      page.navigateToItem(userSelected);
      page.waitTest();
      page.userState().then(function (active) {
        if (active) {
          expect(page.deactivateUser()).toEqual(true)
          expect(page.activateUser()).toEqual(true)
        } else {
          expect(page.activateUser()).toEqual(true)
        }
      });
    })
  });

  it('Se puede editar la información de un cliente', () => {
    page.goToClients();
    page.getPageListCount().then(function (number) {
      let userSelected = Math.floor(Math.random() * (number - 1)) + 1;
      let name = page.getListItemData(userSelected, 1);
      page.navigateToItem(userSelected);
      expect(page.editFieldById('editName', 'Automated Test Name', 'nameDisplay')).toEqual('Automated Test Name');
      expect(page.editFieldById('editEmail', 'autotest@testing.com', 'emailDisplay')).toEqual('E-mail: autotest@testing.com');
      expect(page.editFieldById('editPhone', 3000000000, 'phoneDisplay')).toEqual("Teléfono: 3000000000");
    })
  });

  it('Se puede dar créditos a un cliente', () => {
    page.goToClients();
    page.getPageListCount().then(function (number) {
      let userSelected = Math.floor(Math.random() * (number - 1)) + 1;
      let name = page.getListItemData(userSelected, 1);
      page.navigateToItem(userSelected);
      page.waitTest();
      let credits = Math.ceil(Math.random() * 200000);
      expect(page.creditUser(credits)).toEqual("Creditos: "+credits);
    })
  });
});

describe('Hometap Web: Homies', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  afterEach(() => {
    page.waitTest();
  });

  it('Se muestra la lista de Homies registrados en la aplicación', () => {
    page.goToHomies();
    expect(page.getPageTitle()).toEqual('Homies');
    page.waitTest();
    expect(page.getPageListCount()).toBeGreaterThan(1);
  });

  it('Se puede buscar una Homie', () => {
    // Por nombre
    page.goToHomies();
    page.applySearchHomies("María");
    expect(page.getListItemData(1, 1)).toContain("María");
    // Por telefono
    page.goToHomies();
    page.applySearchHomies("320");
    expect(page.getListItemData(1, 2)).toContain("320");
    // Por ID
    page.goToHomies();
    page.applySearchHomies("TkWBwbBqKNdbgZKsROZRaCAtRW72");
    expect(page.getListItemData(1, 1)).toContain("David Zarate");
  });

  it('Se puede navegar a una Homie', () => {
    page.goToHomies();
    page.getPageListCount().then(function (number) {
      let userSelected = Math.floor(Math.random() * (number - 1)) + 1;
      let name = page.getListItemData(userSelected, 1);
      page.navigateToItem(userSelected);
      expect(page.getPageTitle()).toEqual(name);
    })
  });

  it('Se puede activar/desactivar una Homie', () => {
    page.goToHomies();
    page.getPageListCount().then(function (number) {
      let userSelected = Math.floor(Math.random() * (number - 1)) + 1;
      let name = page.getListItemData(userSelected, 1);
      page.navigateToItem(userSelected);
      page.waitTest();
      page.userState().then(function (active) {
        if (active) {
          expect(page.deactivateUser()).toEqual(true)
          expect(page.activateUser()).toEqual(true)
        } else {
          expect(page.activateUser()).toEqual(true)
        }
      });
    })
  });

  it('Se puede editar la información de una Homie', () => {
    page.goToHomies();
    page.getPageListCount().then(function (number) {
      let userSelected = Math.floor(Math.random() * (number - 1)) + 1;
      let name = page.getListItemData(userSelected, 1);
      page.navigateToItem(userSelected);
      expect(page.editFieldById('editName', 'Automated Test Name', 'nameDisplay')).toEqual('Automated Test Name');
      expect(page.editFieldById('editEmail', 'autotest@testing.com', 'emailDisplay')).toEqual('E-mail: autotest@testing.com');
      expect(page.editFieldById('editPhone', 3000000000, 'phoneDisplay')).toEqual("Teléfono: 3000000000");
    })
  });

  it('Se puede reiniciar el inventario de una Homie', () => {
    page.goToHomies();
    page.getPageListCount().then(function (number) {
      let userSelected = Math.floor(Math.random() * (number - 1)) + 1;
      let name = page.getListItemData(userSelected, 1);
      page.navigateToItem(userSelected);
      page.waitTest();
      let credits = Math.ceil(Math.random() * 200000);
      expect(page.refillHomie().element(by.id('yinv')).getText()).toEqual("100 100 100");
    })
  });
});

describe('Hometap Web: Servicios', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  afterEach(() => {
    page.waitTest();
  });

  it('Se muestra la lista de servicios históricos', () => {
    page.goToServices();
    expect(page.getPageTitle()).toEqual('Servicios');
    page.waitTest();
    expect(page.getPageListCount()).toBeGreaterThan(1);
  });

  it('Se puede buscar un servicio', () => {
    // Por nombre
    page.goToServices();
    page.applySearchServices("Juan");
    expect(page.getListItemData(1, 0)).toContain("Juan");
    // Por fecha
    page.goToServices();
    page.applySearchServices("2017-09-25");
    expect(page.getListItemData(1, 3)).toContain("2017-09-25");
    // Por estado
    page.goToServices();
    page.applySearchServices("Nuevo");
    expect(page.getListItemData(1, 1)).toContain("Nuevo");
    // Por ID
    page.goToServices();
    page.applySearchServices("-Ku2aDgbcQUtf9l3ahhL");
    expect(page.getPageListCount()).toEqual(2);
  });

  it('Se puede navegar a un servicio', () => {
    page.goToServices();
    page.getPageListCount().then(function (number) {
      let userSelected = Math.floor(Math.random() * (number - 1)) + 1;
      let name = page.getListItemData(userSelected, 0);
      page.navigateToItem(userSelected);
      page.waitTest();
      expect(page.getServiceClientName()).toEqual(name);
    })
  });

  it('Se puede cancelar un servicio', () => {
    page.goToServices();
    page.getPageListCount().then(function (number) {
      let userSelected = Math.floor(Math.random() * (number - 1)) + 1;
      page.navigateToItem(userSelected);
      page.waitTest();

      while (!page.canCancelService()) {
        page.goToServices();
        let userSelected = Math.floor(Math.random() * (number - 1)) + 1;
        page.navigateToItem(userSelected);
        page.waitTest();
      }

      expect(page.cancelService()).toEqual(false);
    })
  });

  it('Se puede navegar al cliente', () => {
    page.goToServices();
    page.getPageListCount().then(function (number) {
      let userSelected = Math.floor(Math.random() * (number - 1)) + 1;
      page.navigateToItem(userSelected);
      page.waitTest();
      let name = page.navigateServiceToClient();
      page.waitTest();
      expect(page.getPageTitle()).toEqual(name);
    })
  });

  it('Se puede navegar a la Homie', () => {
    page.goToServices();
    page.getPageListCount().then(function (number) {
      let userSelected = Math.floor(Math.random() * (number - 1)) + 1;
      page.navigateToItem(userSelected);
      page.waitTest();
      let name = page.navigateServiceToHomie();
      page.waitTest();
      expect(page.getPageTitle()).toEqual(name);
    })
  });
});


describe('Hometap Web: Dashboard', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    page.goToFinancial();
  });

  afterEach(() => {
    page.waitTest();
  });

  it('Se muestran los promedios de calificación, servicios, ingresos y otros', () => {
    expect(page.getPageTitle()).toEqual('Finanzas');
    page.waitTest();
    expect(page.getFinanceBoxCount()).toEqual(21);
  });

  it('Se genera un CSV con la información financiera', () => {
    let initial = "2017-09-26"
    let final = "2017-10-16"

    page.downloadFinancialCSV(initial, final)
  });
});
