import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    browser.waitForAngularEnabled(false);
    return browser.get('/dashboard');
  }

  waitTest() {
    browser.driver.sleep(1000)
  }

  login() {
    element(by.id('email')).sendKeys("icoltus@hotmail.com")
    element(by.id('password')).sendKeys("123456")
    element(by.css('.basic-btn')).click()
    browser.driver.sleep(2000)
  }

  goToClients() {
    browser.get('/dashboard/clients/list')
    browser.driver.sleep(1000)
  }

  goToHomies() {
    browser.get('/dashboard/homies/list')
    browser.driver.sleep(1000)
  }

  goToServices() {
    browser.get('/dashboard/services/list')
    browser.driver.sleep(1000)
  }

  goToFinancial() {
    browser.get('/dashboard/finance/general')
    browser.driver.sleep(1000)
  }

  getPageListCount() {
    return element.all(by.id("row")).count()
  }

  applySearchClients(query: string) {
    browser.driver.sleep(1000)
    element.all(by.css('.starIcon')).get(1).click()
    element(by.id('search')).element(by.css('input')).sendKeys(query)
  }

  applySearchHomies(query: string) {
    browser.driver.sleep(1000)
    element.all(by.css('.starIcon')).get(0).click()
    element(by.id('search')).element(by.css('input')).sendKeys(query)
  }

  applySearchServices(query: string) {
    browser.driver.sleep(1000)
    element.all(by.css('.starIcon')).get(1).click()
    element(by.id('search')).element(by.css('input')).sendKeys(query)
  }

  getListItemData(index: number, dataIndex: number) {
    return element.all(by.id("row")).get(index).all(by.css('span')).get(dataIndex).getText()
  }

  navigateToItem(index: number) {
    element.all(by.id("row")).get(index).click()
  }

  userState() {
    return element(by.id('deactivate')).isPresent()
  }

  activateUser() {
    element(by.id('activate')).click()
    browser.driver.sleep(1000)
    element(by.id('confirmActive')).click()
    browser.driver.sleep(1000)
    return element(by.id('deactivate')).isPresent()
  }

  deactivateUser() {
    element(by.id('deactivate')).click()
    browser.driver.sleep(1000)
    element(by.id('confirmActive')).click()
    browser.driver.sleep(1000)
    return element(by.id('activate')).isPresent()
  }

  editFieldById(id: string, data: string | number, verificationID: string) {
    element(by.id('edit')).click()
    browser.driver.sleep(1000)
    let oldData = element(by.id(id)).getText()
    element(by.id(id)).clear()
    browser.driver.sleep(1000)
    element(by.id(id)).sendKeys(data)
    element(by.id('saveEdit')).click()
    browser.driver.sleep(2000)
    let newData = element(by.id(verificationID)).getText()
    // Revert
    element(by.id('edit')).click()
    browser.driver.sleep(1000)
    element(by.id(id)).clear().then(function() {
      element(by.id(id)).sendKeys(oldData)
      element(by.id('saveEdit')).click()
      browser.driver.sleep(500)
    })
    return newData
  }

  creditUser(credits: number) {
    element(by.id('inventory')).element(by.css('span')).click()
    browser.driver.sleep(500)
    element(by.id('clientCredits')).clear().then(function() {
      element(by.id('clientCredits')).sendKeys(credits)
    })
    browser.driver.sleep(1000)
    element(by.id('confirmClientCredit')).click()
    browser.driver.sleep(500)
    return element(by.id('inventory')).element(by.css('span')).getText()
  }

  refillHomie() {
    element(by.id('inventory')).element(by.css('span')).click()
    browser.driver.sleep(500)
    element(by.id('confirmInventory')).click()
    browser.driver.sleep(500)
    return element(by.id('inventory'))
  }

  canCancelService() {
    return (element(by.id('cancelService')).isPresent())
  }

  cancelService() {
    element(by.id('cancelService')).click()
    browser.driver.sleep(500)
    element(by.id('confirmCancel')).click()
    browser.driver.sleep(500)
    return (element(by.id('cancelService')).isPresent())
  }

  navigateServiceToHomie() {
    let name = element(by.id('navigateHomie')).getText()
    element(by.id('navigateHomie')).click()
    browser.driver.sleep(1000)
    return name
  }

  navigateServiceToClient() {
    let name = element(by.id('navigateClient')).getText()
    element(by.id('navigateClient')).click()
    browser.driver.sleep(1000)
    return name
  }

  getServiceClientName() {
    return element(by.id('navigateClient')).getText()
  }

  getPageTitle() {
    return element(by.css('h1')).getText()
  }

  getFinanceBoxCount() {
    return element.all(by.css('span')).count()
  }

  downloadFinancialCSV(initial: string, final: string) {
    var filename = '/e2e/downloads/'+initial+'_'+final+'.csv';
    var fs = require('fs');
    
    if (fs.existsSync(filename)) {
        // Make sure the browser doesn't have to rename the download.
        fs.unlinkSync(filename);
    }
    
    element(by.id('downloadCSV')).click()
    
    browser.driver.wait(function() {
        // Wait until the file has been downloaded.
        // We need to wait thus as otherwise protractor has a nasty habit of
        // trying to do any following tests while the file is still being
        // downloaded and hasn't been moved to its final location.
        return fs.existsSync(filename);
    }, 5000).then(function() {
        // Do whatever checks you need here.  This is a simple comparison;
        // for a larger file you might want to do calculate the file's MD5
        // hash and see if it matches what you expect.
        expect(fs.readFileSync(filename, { encoding: 'utf8' })).toContain(
            "Tipo de Dato,Minimo,Maixmo,Moda,Total\n"
        );
    });
  }
}
