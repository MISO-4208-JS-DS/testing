Feature: CRUD para medios de pago
    Scenario: Agrego un nuevo medio de pago
        Given I wait for the "MainActivity" screen to appear
        When I see "Inicio"
        And I drag from 90:50 to 30:50 moving with 1 steps
        And I drag from 90:50 to 30:50 moving with 1 steps
        And I press "Ver medios de pago"
        Then I press "Agregar nuevo medio de pago"
        And I enter "Tarjeta MISO" into input field number 1
        And I enter "4222740000427067" into input field number 2
        And I enter "06/19" into input field number 3
        And I enter "999" into input field number 4
        Then I press "Guardar Tarjeta"
        And I wait for 15 seconds
        Then I should see "Visa ***7067"