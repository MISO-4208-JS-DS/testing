Feature: Agendar un nuevo servicio
    Scenario: Agendar un servicio
        Given I wait for the "MainActivity" screen to appear
        When I see "Inicio"
        And I press "home_services_fab"
        Then I press "Fecha"
        And I wait for 5 seconds 
        # Uso de librerías externas que no son detectadas, clic en aceptar.
        Then I press "Hora"
        And I wait for 5 seconds 
        # Uso de librerías externas que no son detectadas, clic 7am y en aceptar.
        Then I scroll down
        And I press "Siguiente"
        Then I wait for 10 seconds
        And I press "María Silva Rodríguez"
        Then I see "Lo que piensan de tu Homie"
        And I press "Confirmar Selección"
        Then I see "City U"
        And I press "Continuar"
        Then I see "65,000 COP"
        And I press "Confirmar"
        # Llegamos al paso de las tarjetas de crédito, de donde no podemos pasar sin pagar
