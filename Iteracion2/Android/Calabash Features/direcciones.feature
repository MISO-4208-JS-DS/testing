Feature: CRUD de direcciones
    Scenario: Agrego una dirección
        Given I wait for the "MainActivity" screen to appear
        When I see "Inicio"
        And I drag from 30:50 to 90:50 moving with 1 steps
        And I see "Direcciones"
        Then I press "Agregar una nueva dirección"
        And I enter "MISO Pruebas" into input field number 1
        And I press "Dirección"
        Then I wait for 8 seconds
        # Acá hay que ayudar a Calabash haciendo clic en una ubicación de maps rápidamente
        Then I enter "2" into input field number 3
        And I enter "120" into input field number 4
        And I enter "120" into input field number 5
        And I enter "1" into input field number 6
        And I enter "2" into input field number 7
        Then I press "No"
        And I press "Guardar"
        Then I wait for the "MainActivity" screen to appear
        And I wait for 3 seconds
        Then I should see "MISO Pruebas"
    
    Scenario: Edito una dirección
        Given I wait for the "MainActivity" screen to appear
        When I see "Inicio"
        And I drag from 30:50 to 90:50 moving with 1 steps
        And I see "Direcciones"
        Then I press "MISO Pruebas"
        And I clear input field number 1
        And I enter "MISO Editadas" into input field number 1
        And I press "Guardar"
        Then I wait for the "MainActivity" screen to appear
        And I wait for 3 seconds
        Then I don't see "MISO Pruebas"
        And I should see "MISO Editadas"
