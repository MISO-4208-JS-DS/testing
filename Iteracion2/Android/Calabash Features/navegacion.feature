Feature: Navegación Básica
  Este feature me permite evaluar la navegación entre pestañas de la aplicación
  usando los drags en el ViewPager principa; y entrando a ciertas secciones no profundas.

  Scenario: Navego a la pestaña de Direcciones desde inicio
    Given I wait for the "MainActivity" screen to appear
    When I see "Inicio"
    And I drag from 30:50 to 90:50 moving with 1 steps
    Then I should see "Direcciones"

  Scenario: Navego a la pestaña de Historial desde Inicio
    Given I wait for the "MainActivity" screen to appear
    When I see "Inicio"
    And I drag from 90:50 to 30:50 moving with 1 steps
    Then I should see "Historial"

  Scenario: Navego a la pestaña de Perfil desde Inicio
    Given I wait for the "MainActivity" screen to appear
    When I see "Inicio"
    And I drag from 90:50 to 30:50 moving with 1 steps
    And I drag from 90:50 to 30:50 moving with 1 steps
    Then I should see "Perfil"

  Scenario: Veo una de mis direcciones registradas
    Given I wait for the "MainActivity" screen to appear
    When I see "Inicio"
    And I drag from 30:50 to 90:50 moving with 1 steps
    And I see "Direcciones"
    And I press "City U"
    And I see "1602"
    Then I should see "Guardar"

  Scenario: Abro un servicio de la pantalla de inicio
    Given I wait for the "MainActivity" screen to appear
    When I see "Inicio"
    And I press "Miranda Losantos"
    And I see "Homie"
    Then I should see "Miranda Losantos"

  Scenario: Abro un servicio completado de la pantalla de historial
    Given I wait for the "MainActivity" screen to appear
    When I see "Inicio"
    And I drag from 90:50 to 30:50 moving with 1 steps
    And I press "Jana María Chaparro"
    And I see "Estado"
    Then I should see "Completado"

  Scenario: Veo mis medios de pago desde el Perfil
    Given I wait for the "MainActivity" screen to appear
    When I see "Inicio"
    And I drag from 90:50 to 30:50 moving with 1 steps
    And I drag from 90:50 to 30:50 moving with 1 steps
    And I press "Ver medios de pago"
    And I see "Visa ***7067"
    Then I should see "Agregar nuevo medio de pago"

  Scenario: Veo mis medios créditos desde el Perfil
    Given I wait for the "MainActivity" screen to appear
    When I see "Inicio"
    And I drag from 90:50 to 30:50 moving with 1 steps
    And I drag from 90:50 to 30:50 moving with 1 steps
    And I scroll down
    And I press "Créditos"
    And I see "95917 COP"
    Then I should see "Redimir Cupón"