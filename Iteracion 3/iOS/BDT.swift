//
//  HometapUITests.swift
//  HometapUITests
//
//  Created by Daniel Soto on 9/13/17.
//  Copyright © 2017 Tres Astronautas. All rights reserved.
//

import XCTest
import XCTest_Gherkin

class BDDTests: XCTestCase {
    
    func testLogin() {
        Given("I have launched the app")
        When("I see Google")
        And("I tap Google")
        Then("I should see homescreen")
    }
    
    func testAddress() {
        Given("I have launched the app")
        When("I scroll left")
        And("I tap Agregar nueva dirección")
        And("I fill in the data")
        And("I tap Guardar")
        Then("I should see new place")
    }
    
    func testPayment() {
        Given("I have launched the app")
        When("I scroll rigth")
        And("I tap Agregar nuevo medio de pago")
        And("I fill in the payment")
        And("I tap Guardar")
        Then("I should see new payment")
    }
    
    func testProfilePicture() {
        Given("I have launched the app")
        When("I scroll rigth")
        And("I tap my photo")
        And("I agree to camera")
        And("I choose a photo")
        Then("I should see new photo")
    }
    
    func testProfileUpdate() {
        Given("I have launched the app")
        When("I scroll rigth")
        And("I change my data")
        Then("I see my new data")
    }
    
    func testServiceBooked() {
        Given("I have launched the app")
        When("I start booking")
        And("I tap Continuar")
        And("I select a Homie")
        And("I tap Confirmar")
        And("I tap Continuar")
        And("I tap Confirmar")
        And("I pay")
        Then("I see Tu servicio ha sido agendado")
    }
    
    func testServiceCanceled() {
        Given("I have launched the app")
        When("I open a service")
        And("I tap Cancelar")
        Then("I see status canceled")
    }
    
    func testCreditsRedeemed() {
        Given("I have launched the app")
        When("I scroll rigth")
        And("I tap Créditos")
        And("I tap Redimir Bono")
        And("I enter credts info")
        Then("I should see my new credits")
    }
    
    func testServiceCompleted() {
        Given("I have launched the app")
        When("I go to history")
        And("I open a service")
        Then("I see status completed")
    }
    
}
