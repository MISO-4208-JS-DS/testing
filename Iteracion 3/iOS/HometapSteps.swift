//
//  StepsDefinitions.swift
//  HometapUITests
//
//  Created by Daniel Soto on 11/15/17.
//  Copyright © 2017 Tres Astronautas. All rights reserved.
//

import Foundation
import XCTest
import XCTest_Gherkin

class HometapSteps : StepDefiner {
    var app: XCUIApplication!
    override func defineSteps() {
        step("I have launched the app") {
            XCUIApplication().launch()
            self.app = XCUIApplication()
        }
        
        step("I see Google") {
            let t = self.app.buttons["Ingresa con Google"].exists
            XCTAssertEqual(t, true)
        }
        
        step("I tap Google") {
            self.app!.buttons["google c b"].tap()
            if self.app.alerts["“HomeTap” Wants to Use “google.com” to Sign In"].exists {
                self.app.alerts["“HomeTap” Wants to Use “google.com” to Sign In"].collectionViews.buttons["Continue"].tap()
            }
        }
        
        step("I should see homescreen") {
            let t = self.app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element(boundBy: 1).exists
            XCTAssertEqual(t, true)
            
        }

        step("I scroll left") {
            // Navigate to Address by tap
            let window = self.app.children(matching: .window).element(boundBy: 0)
            window.children(matching: .other).element.children(matching: .other).element(boundBy: 2).children(matching: .other).element(boundBy: 1).children(matching: .button).element(boundBy: 0).tap()
        }
        
        step("I tap Agregar nueva dirección") {
            // Add address
            let tablesQuery = self.app.tables
            tablesQuery.buttons["Agregar una nueva dirección"].tap()
        }
        
        step("I fill in the data") {
            // For scrollable content
            let elementsQuery = self.app.scrollViews.otherElements
            
            // Nickname
            let nicknameText = elementsQuery.textFields["Mi Casa"]
            nicknameText.tap()
            nicknameText.typeText("Automated Test House")
            // Address
            elementsQuery.textFields["Calle 100 # 7 - 0"].tap()
            
            // On Google Place Picker
            let searchbarNavigationBar = self.app.navigationBars["searchBar"]
            self.app.navigationBars["searchBar"].buttons["Cancel"].tap()
            searchbarNavigationBar.searchFields["Search"].typeText("Torre San Marino")
            elementsQuery.tables.cells.element(boundBy: 2).tap()
            
            // Number
            let numberText = elementsQuery.textFields["401"]
            numberText.tap()
            numberText.typeText("3")
            // Meters
            let metersText = elementsQuery.textFields["143 m2"]
            metersText.tap()
            metersText.typeText("120")
            // Floors
            let element = self.app.scrollViews.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 3).children(matching: .other).element
            let floorsText = element.children(matching: .other).element(boundBy: 1).textFields["3"]
            floorsText.tap()
            floorsText.typeText("2")
            // Rooms
            let roomsText = elementsQuery.textFields["4"]
            roomsText.tap()
            roomsText.typeText("5")
            // Baths
            let bathroomsText = element.children(matching: .other).element(boundBy: 3).textFields["3"]
            bathroomsText.tap()
            bathroomsText.typeText("3")
            // Wifi
            let wifiText = elementsQuery.textFields["Clave WiFi"]
            wifiText.tap()
            wifiText.typeText("AutomaticWifiPassword")
        }
        
        step("I tap Guardar") {
            self.app.buttons["Guardar"].tap()
        }
        
        step("I should see new place") {
            let tablesQuery = self.app.tables
           let t = tablesQuery.children(matching: .cell).element(boundBy: 0).staticTexts["Automated Test House"].exists
            
            XCTAssertEqual(t, true)
        }
        
        step("I scroll rigth") {
            let window = self.app.children(matching: .window).element(boundBy: 0)
            window.children(matching: .other).element.children(matching: .other).element(boundBy: 2).children(matching: .other).element(boundBy: 1).children(matching: .button).element(boundBy: 3).tap()
        }
        
        step("I tap Agregar nuevo medio de pago") {
            // Add payment
            let elementsQuery = self.app.scrollViews.otherElements
            elementsQuery.buttons["Ver medios de pago"].tap()
            let tablesQuery = self.app.tables
            tablesQuery.buttons["Agregar un nuevo medio de pago"].tap()
        }
        
        step("I fill in the payment") {
            let elementsQuery = self.app.scrollViews.otherElements

            // Name
            let nombreCompletoTextField = elementsQuery.textFields["Nombre completo"]
            nombreCompletoTextField.tap()
            nombreCompletoTextField.typeText("Daniel Soto")
            // Number
            let numberText = elementsQuery.textFields["1234 5678 9123 4567"]
            numberText.tap()
            numberText.typeText("5306956676130183")
            // Date
            let dateText = elementsQuery.textFields["01/30"]
            dateText.tap()
            dateText.typeText("0718")
            // CVC
            let cvcText = elementsQuery.textFields["123"]
            cvcText.tap()
            cvcText.typeText("436")
        }
        
        step("I should see new payment") {
            let tablesQuery = self.app.tables
            let t = tablesQuery.children(matching: .cell).element(boundBy: 0).staticTexts["MASTERCARD ***0183"].exists
            XCTAssertEqual(t, true)
        }
        
        step("I tap my photo") {
            // Navigate inside profile
            let scrollViewsQuery = self.app.scrollViews
            // Change photo
            scrollViewsQuery.otherElements.containing(.staticText, identifier:"Perfil").children(matching: .button).element(boundBy: 0).tap()
        }
        
        step("I agree to camera") {
            if self.app.alerts["“HomeTap” Would Like to Access the Camera"].exists {
                self.app.alerts["“HomeTap” Would Like to Access the Camera"].collectionViews.buttons["OK"].tap()
            }
            if self.app.alerts["“HomeTap” Would Like to Access Your Photos"].exists {
                self.app.alerts["“HomeTap” Would Like to Access Your Photos"].collectionViews.buttons["OK"].tap()
            }
            if self.app.alerts["Necesitamos tu permiso"].exists {
                self.app.alerts["Necesitamos tu permiso"].collectionViews.buttons["Listo"].tap()
            }
        }
        
        step("I choose a photo") {
            self.app.collectionViews.children(matching: .cell).matching(identifier: "Photo").element(boundBy: 0).tap()
            self.app.buttons["Listo"].tap()
        }
        
        step("I should see new photo") {
            // Navigate inside profile
            let scrollViewsQuery = self.app.scrollViews
            
            // Change photo
            let t = scrollViewsQuery.otherElements.containing(.staticText, identifier:"Perfil").children(matching: .button).element(boundBy: 0).exists
            XCTAssertTrue(t)
        }
        
        step("I change my data") {
            let scrollViewsQuery = self.app.scrollViews
            let elementsQuery = scrollViewsQuery.otherElements
            
            // Change name
            var nameButton = elementsQuery.buttons["Daniel Soto Rey"]
            XCTAssertEqual(nameButton.exists, true)
            nameButton.tap()
            
            var datoTextField = self.app.textFields["Daniel Soto Rey"]
            datoTextField.tap()
            datoTextField.clearAndEnterText(text: "Automated Test Name")
            
            self.app.buttons["Guardar Cambios"].tap()
            
            XCTAssertEqual(elementsQuery.buttons["Automated Test Name"].exists, true)
            
            // Reverse name
            nameButton = elementsQuery.buttons["Automated Test Name"]
            XCTAssertEqual(nameButton.exists, true)
            nameButton.tap()
            
            datoTextField = self.app.textFields["Automated Test Name"]
            datoTextField.tap()
            datoTextField.clearAndEnterText(text: "Daniel Soto Rey")
            
            self.app.buttons["Guardar Cambios"].tap()
                        XCTAssertEqual(elementsQuery.buttons["Daniel Soto Rey"].exists, true)
            
            // Change phone
            var phoneButton = elementsQuery.buttons["3017303973"]
            XCTAssertEqual(phoneButton.exists, true)
            phoneButton.tap()
            
            datoTextField = self.app.textFields["3017303973"]
            datoTextField.tap()
            datoTextField.clearAndEnterText(text: "3000000000")
            
            self.app.buttons["Guardar Cambios"].tap()
            
            XCTAssertEqual(elementsQuery.buttons["3000000000"].exists, true)
            
            // Reverse phone
            phoneButton = elementsQuery.buttons["3000000000"]
            XCTAssertEqual(phoneButton.exists, true)
            phoneButton.tap()
            
            datoTextField = self.app.textFields["3000000000"]
            datoTextField.tap()
            datoTextField.clearAndEnterText(text: "3017303973")
            
            self.app.buttons["Guardar Cambios"].tap()
                        XCTAssertEqual(elementsQuery.buttons["3017303973"].exists, true)
            // Change email
            var emailButton = elementsQuery.buttons["dansotorey@gmail.com"]
            XCTAssertTrue(emailButton.exists)
            emailButton.tap()
            
            datoTextField = self.app.textFields["dansotorey@gmail.com"]
            datoTextField.tap()
            datoTextField.clearAndEnterText(text: "automated@test.com")
            
            self.app.buttons["Guardar Cambios"].tap()
                        XCTAssertTrue(elementsQuery.buttons["automated@test.com"].exists)
            
            // Reverse email
            emailButton = elementsQuery.buttons["automated@test.com"]
            XCTAssertTrue(emailButton.exists)
            emailButton.tap()
            
            datoTextField = self.app.textFields["automated@test.com"]
            datoTextField.tap()
            datoTextField.clearAndEnterText(text: "dansotorey@gmail.com")
            
            self.app.buttons["Guardar Cambios"].tap()
        }
        
        step("I see my new data") {
            let scrollViewsQuery = self.app.scrollViews
            let elementsQuery = scrollViewsQuery.otherElements
            // Reverse name
            let t = elementsQuery.buttons["dansotorey@gmail.com"].exists
            XCTAssertEqual(t, true)
        }
        
        step("I start booking") {
            // Start Booking
            self.app.buttons["iconAddService"].tap()
        }
        
        step("I tap Continuar") {
            let scrollViewsQuery = self.app.scrollViews
            
            // Find Homies
            let elementsQuery = scrollViewsQuery.otherElements
            elementsQuery.buttons["Continuar"].tap()
        }
        
        step("I select a Homie") {
            // Chose first Homie
            let window = self.app.children(matching: .window).element(boundBy: 0)
            let homie1 = window.children(matching: .other).element(boundBy: 1)
            XCTAssertTrue(homie1.exists)
            homie1.tap()
        }
        
        step("I tap Confirmar") {
            // Confirm Homie
            let scrollViewsQuery = self.app.scrollViews
            let elementsQuery = scrollViewsQuery.otherElements
            elementsQuery.buttons["Confirmar"].tap()
        }
        
        step("I pay") {
            // Pay
            XCTAssertTrue(self.app.buttons["Pagar"].exists)
            self.app.buttons["Pagar"].tap()
        }
        
        step("I see Tu servicio ha sido agendado") {
            // Wait for booking confirmation
            XCTAssertTrue(self.app.otherElements["¡Tu servicio ha sido agendado correctamente!"].exists)
        }
        
        step("I open a service") {
            self.app.tables.cells["0"].tap()
        }
        
        step("I tap Cancelar") {
            let scrollViewsQuery = self.app.scrollViews
            let elementsQuery = scrollViewsQuery.otherElements
            elementsQuery.buttons["Cancelar"].tap()
        }
        
        step("I see status canceled") {
            let scrollViewsQuery = self.app.scrollViews
            let elementsQuery = scrollViewsQuery.otherElements
            XCTAssertTrue(elementsQuery["Cancelado"].exists)
        }
        
        step("I tap Créditos") {
            let elementsQuery = self.app.scrollViews.otherElements
            elementsQuery.buttons["Creditos"].tap()
        }
        
        step("I tap Redimir Bono") {
           self.app.buttons["Redimir bono"].tap()
        }
        
        step("I enter credts info") {
            let hometapTextField = self.app.textFields["Hometap"]
            hometapTextField.tap()
            hometapTextField.typeText("HometapCouponTest")
            self.app.buttons["Redimir"].tap()
        }
        
        step("I should see my new credits") {
            XCTAssertTrue(self.app.scrollViews.otherElements["10000 COP"].exists)
        }
        
        step("I go to history") {
            let window = self.app.children(matching: .window).element(boundBy: 0)
            window.children(matching: .other).element.children(matching: .other).element(boundBy: 2).children(matching: .other).element(boundBy: 1).children(matching: .button).element(boundBy: 2).tap()
        }
        
        step("I see status completed") {
            let scrollViewsQuery = self.app.scrollViews
            let elementsQuery = scrollViewsQuery.otherElements
            XCTAssertTrue(elementsQuery["Completado"].exists)
        }
    
    }
}
