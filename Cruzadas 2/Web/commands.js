// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
};


Cypress.Commands.add("login", (user, pass) => {
    cy.visit("https://emmalabsqa.herokuapp.com/")
    cy.get('.content').contains('Iniciar Sesión').click()
    cy.get('.content').get('#id_username_login').click().type(user)
    cy.get('#id_password_login').click().type(pass)
    cy.get('.modal-footer').contains('Iniciar Sesión').click()
})

Cypress.Commands.add("monkey", (events) => {
    cy.document().then(function(document) {
        var left = events;
        if (left > 0) {
            let eventType = getRandomInt(1, 10);
            if (eventType <= 7) {
                if (document.getElementsByTagName("a").length > 0) {
                    cy.get('a').then($links => {
                        var link = $links.get(getRandomInt(0, $links.length));
                        if (!Cypress.dom.isHidden(link)) {
                            cy.wrap(link).click({
                                force: true
                            });
                        }
                    });
                }
            } else if (eventType == 8) {
                if (document.getElementsByTagName("input").length > 0) {
                    cy.get('input[type="text"]').then($inputs => {
                        var input = $inputs.get(getRandomInt(0, $inputs.length));
                        if (!Cypress.dom.isHidden(input)) {
                            cy.wrap(input).click({
                                force: true
                            }).type('TestTestText', {
                                force: true
                            });
                        }
                    });
                }
            } else if (eventType == 9) {
                if (document.getElementsByTagName("select").length > 0) {
                    cy.get('select').then($combos => {
                        var combo = $combos.get(getRandomInt(0, $combos.length));
                        if (!Cypress.dom.isHidden(combo)) {
                            let options = combo.children
                            let opt = getRandomInt(0, options.length)
                            cy.wrap(combo).select(options[opt].value, {
                                force: true
                            });
                        }
                    });
                }
            } else {
                if (document.getElementsByTagName("button").length > 0) {
                    cy.get('button').then($buttons => {
                        var button = $buttons.get(getRandomInt(0, $buttons.length));
                        if (!Cypress.dom.isHidden(button)) {
                            cy.wrap(button).click({
                                force: true
                            });
                        }
                    });
                }
    
            }
            cy.wait(1000)
            cy.monkey(left-1);
        }
    })
})

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

// Cypress.on('fail', (err, runnable) => {
//     debugger

//     console.log(err);
// })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
