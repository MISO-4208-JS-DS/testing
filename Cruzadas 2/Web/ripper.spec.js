let id = Math.random()*100;
describe('Ripper: EmmaLabs', function() {
    it('Busca errores como científico', function() {
        cy.login('cientifico', '1a2s3d4f5g')
        cy.monkey(10000)
    })
    it('Busca errores como asistente', function() {
        cy.login('asistente', '1a2s3d4f5g')
        cy.monkey(10000)
    }),
    it('Busca errores como jefe', function() {
        cy.login('jefe', '1a2s3d4f5g')
        cy.monkey(10000)
    })
})
