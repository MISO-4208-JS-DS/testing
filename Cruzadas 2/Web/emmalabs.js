let id = Math.random() * 100;

describe('Emma Labs: Cientifico', function () {
    it('Inicia sesión como científico', function () {
        cy.login('cientifico', '1a2s3d4f5g')
    }),
    it('Crea orden de compra a cientifico', function () {
        cy.login('cientifico', '1a2s3d4f5g')
        cy.contains('Solicitudes').click()
        cy.contains('Gestionar Solicitudes').click()
        cy.contains('Crea una Solicitud').click()
        cy.get('#id_titulo').click().type('Orden generada por Cypress')
        cy.get('#id_tipo').select('1')
        cy.get('#id_texto').click().type('Mensaje de la orden de compra generada por Cypress. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Y más Pruebas.')
        cy.get('#id_usuario_destino').select('1')
        cy.contains('Guardar').click()
        cy.contains('La solicitud se ha creado correctamente')
    }),
    it('Crea orden de compra a asistente', function () {
        cy.login('cientifico', '1a2s3d4f5g')
        cy.contains('Solicitudes').click()
        cy.contains('Gestionar Solicitudes').click()
        cy.contains('Crea una Solicitud').click()
        cy.get('#id_titulo').click().type('Orden generada por Cypress')
        cy.get('#id_tipo').select('1')
        cy.get('#id_texto').click().type('Mensaje de la orden de compra generada por Cypress. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Y más Pruebas.')
        cy.get('#id_usuario_destino').select('2')
        cy.contains('Guardar').click({
            force: true
        })
        cy.contains('La solicitud se ha creado correctamente')
    }),
    it('Crea orden de compra a jefe', function () {
        cy.login('cientifico', '1a2s3d4f5g')
        cy.contains('Solicitudes').click()
        cy.contains('Gestionar Solicitudes').click()
        cy.contains('Crea una Solicitud').click()
        cy.get('#id_titulo').click().type('Orden generada por Cypress')
        cy.get('#id_tipo').select('1')
        cy.get('#id_texto').click().type('Mensaje de la orden de compra generada por Cypress. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Y más Pruebas.')
        cy.get('#id_usuario_destino').select('3')
        cy.contains('Guardar').click({
            force: true
        })
        cy.contains('La solicitud se ha creado correctamente')
    }),
    it('Crea orden de compra frustrada', function () {
        cy.login('cientifico', '1a2s3d4f5g')
        cy.contains('Solicitudes').click()
        cy.contains('Gestionar Solicitudes').click()
        cy.contains('Crea una Solicitud').click()
        cy.get('#id_titulo').click().type('NULL @=-0124[[]]')
        cy.get('#id_texto').click().type('Mensaje de la orden de compra generada por Cypress. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Y más Pruebas.')
        cy.contains('Guardar').click({
            force: true
        })
        cy.wait(1000)
        cy.get('html').should('not.contain', 'La solicitud se ha creado correctamente')  
    }),
    it('Crea orden de compra frustrada 2', function () {
        cy.login('cientifico', '1a2s3d4f5g')
        cy.contains('Solicitudes').click()
        cy.contains('Gestionar Solicitudes').click()
        cy.contains('Crea una Solicitud').click()
        cy.get('#id_tipo').select('1')
        cy.get('#id_usuario_destino').select('1')
        cy.contains('Guardar').click({
            force: true
        })
        cy.wait(1000)
        cy.get('html').should('not.contain', 'La solicitud se ha creado correctamente')  
    }),
    it('Navega a las solicitudes', function () {
        cy.login('cientifico', '1a2s3d4f5g')
        cy.contains('Solicitudes').click()
        cy.contains('Gestionar Solicitudes').click()
        cy.contains('Revisa tus solicitudes').click()
        cy.get('tbody>tr>td').then($tds => {
            var name = $tds.get(0).val()
            $tds.get(4).get('a').click()
            cy.get('.panel-title').should('contain', name)
        });
    })
})

describe('Emma Labs: Asistente', function () {
    it('Inicia sesión como asistente', function () {
        cy.login('asistente', '1a2s3d4f5g')
    }),
    it('Crea orden de compra a cientifico', function () {
        cy.login('asistente', '1a2s3d4f5g')
        cy.contains('Solicitudes').click()
        cy.contains('Gestionar Solicitudes').click()
        cy.contains('Crea una Solicitud').click({
            force: true
        })
        cy.get('#id_titulo').click().type('Orden generada por Cypress')
        cy.get('#id_tipo').select('1')
        cy.get('#id_texto').click().type('Mensaje de la orden de compra generada por Cypress. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Y más Pruebas.')
        cy.get('#id_usuario_destino').select('1')
        cy.contains('Guardar').click({
            force: true
        })
        cy.contains('La solicitud se ha creado correctamente')
    }),
    it('Crea orden de compra a asistente', function () {
        cy.login('asistente', '1a2s3d4f5g')
        cy.contains('Solicitudes').click()
        cy.contains('Gestionar Solicitudes').click()
        cy.contains('Crea una Solicitud').click()
        cy.get('#id_titulo').click().type('Orden generada por Cypress')
        cy.get('#id_tipo').select('1')
        cy.get('#id_texto').click({
            force: true
        }).type('Mensaje de la orden de compra generada por Cypress. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Y más Pruebas.')
        cy.get('#id_usuario_destino').select('2')
        cy.contains('Guardar').click({
            force: true
        })
        cy.contains('La solicitud se ha creado correctamente')
    }),
    it('Crea orden de compra a jefe', function () {
        cy.login('asistente', '1a2s3d4f5g')
        cy.contains('Solicitudes').click()
        cy.contains('Gestionar Solicitudes').click()
        cy.contains('Crea una Solicitud').click()
        cy.get('#id_titulo').click().type('Orden generada por Cypress')
        cy.get('#id_tipo').select('1')
        cy.get('#id_texto').click().type('Mensaje de la orden de compra generada por Cypress. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Y más Pruebas.')
        cy.get('#id_usuario_destino').select('3')
        cy.contains('Guardar').click()
        cy.contains('La solicitud se ha creado correctamente')
    }),
    it('Crea orden de compra frustrada', function () {
        cy.login('asistente', '1a2s3d4f5g')
        cy.contains('Solicitudes').click()
        cy.contains('Gestionar Solicitudes').click()
        cy.contains('Crea una Solicitud').click()
        cy.get('#id_titulo').click().type('NULL @=-0124[[]]')
        cy.get('#id_texto').click().type('Mensaje de la orden de compra generada por Cypress. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Pruebas. Y más Pruebas.')
        cy.contains('Guardar').click()
        cy.wait(1000)
        cy.get('html').should('not.contain', 'La solicitud se ha creado correctamente')  
    }),
    it('Crea orden de compra frustrada 2', function () {
        cy.login('asistente', '1a2s3d4f5g')
        cy.contains('Solicitudes').click()
        cy.contains('Gestionar Solicitudes').click()
        cy.contains('Crea una Solicitud').click({
            force: true
        })
        cy.get('#id_tipo').select('1')
        cy.get('#id_usuario_destino').select('1')
        cy.contains('Guardar').click({
            force: true
        })
        cy.wait(1000)
        cy.get('html').should('not.contain', 'La solicitud se ha creado correctamente')  
    }),
    it('Realiza un experimento pendiente', function () {
        cy.login('asistente', '1a2s3d4f5g')
        cy.contains('Experimentos').click()
        cy.contains('Ver Experimentos').click()
        cy.get('.panel-body').click()
        cy.contains('Realizar ').click({
            force: true
        })
        cy.get('.modal-body').find('select[name="resultado"]').select('Pendiente', {force: true})
        cy.contains('Guardar').click({
            force: true
        })
        cy.get('html').should('contain', 'Agregar resultados')
    }),
    it('Realiza un experimento exitoso', function () {
        cy.login('asistente', '1a2s3d4f5g')
        cy.contains('Experimentos').click()
        cy.contains('Ver Experimentos').click()
        cy.get('.panel-body').click()
        cy.contains('Realizar ').click({
            force: true
        })
        cy.wait(1000)
        cy.get('.modal-body').find('select[name="resultado"]').select('Exitoso', {force: true})
        cy.contains('Guardar').click({
            force: true
        })
        cy.get('html').should('contain', 'Agregar resultados')
        cy.get('html').should('contain', 'Exitoso')        
    }),
    it('Realiza un experimento fallido', function () {
        cy.login('asistente', '1a2s3d4f5g')
        cy.contains('Experimentos').click()
        cy.contains('Ver Experimentos').click()
        cy.get('.panel-body').click()
        cy.contains('Realizar ').click({
            force: true
        })
        cy.wait(1000)
        cy.get('.modal-body').find('select[name="resultado"]').select('Fallido', {force: true})
        cy.contains('Guardar').click({
            force: true
        })
        cy.get('html').should('contain', 'Agregar resultados')
        cy.get('html').should('contain', 'Fallido')        
    })
})

describe('Emma Labs: Jefe', function () {
    it('Inicia sesión como jefe', function () {
        cy.login('jefe', '1a2s3d4f5g')
    }),
    it('Realiza un experimento pendiente', function () {
        cy.login('jefe', '1a2s3d4f5g')
        cy.contains('Experimentos').click()
        cy.contains('Ver Experimentos').click()
        cy.get('.panel-body').click()
        cy.contains('Realizar ').click({
            force: true
        })
        cy.wait(1000)
        cy.get('.modal-body').find('select[name="resultado"]').select('Pendiente', {force: true})
        cy.contains('Guardar').click({
            force: true
        })
        cy.get('html').should('contain', 'Agregar resultados')
    }),
    it('Realiza un experimento exitoso', function () {
        cy.login('jefe', '1a2s3d4f5g')
        cy.contains('Experimentos').click()
        cy.contains('Ver Experimentos').click()
        cy.get('.panel-body').click()
        cy.contains('Realizar ').click({
            force: true
        })
        cy.wait(1000)
        cy.get('.modal-body').find('select[name="resultado"]').select('Exitoso', {force: true})
        cy.contains('Guardar').click({
            force: true
        })
        cy.get('html').should('contain', 'Agregar resultados')
        cy.get('html').should('contain', 'Exitoso')        
    }),
    it('Realiza un experimento fallido', function () {
        cy.login('jefe', '1a2s3d4f5g')
        cy.contains('Experimentos').click()
        cy.contains('Ver Experimentos').click()
        cy.get('.panel-body').click()
        cy.contains('Realizar ').click({
            force: true
        })
        cy.wait(1000)
        cy.get('.modal-body').find('select[name="resultado"]').select('Fallido', {force: true})
        cy.contains('Guardar').click({
            force: true
        })
        cy.get('html').should('contain', 'Agregar resultados')
        cy.get('html').should('contain', 'Fallido')        
    }),
    it('Agrega resultados experimento', function () {
        cy.login('jefe', '1a2s3d4f5g')
        cy.contains('Experimentos').click()
        cy.contains('Ver Experimentos').click()
        cy.get('.panel-body').click()
        cy.contains('Agregar resultados').click({
            force: true
        })
        cy.get('.table').find('select[name="proyecto"]').select('1', {force: true})
        cy.get('#datepicker-autoclose').click({force: true}).type('11/08/2017')
        cy.get('.table').find('select[name="protocolo"]').select('1', {force: true})
        cy.get('.table').find('input[name="resultados"]').click({force: true}).type('Resultados obtenidos con Cypress')
        cy.get('.table').find('input[type="radio"]').check('True', {force: true})
        cy.get('.table').find('input[name="observaciones"]').click({force: true}).type('Observaciones obtenidas con Cypress')
        cy.contains('Guardar ').click({
            force: true
        })
        cy.get('html').should('contain', 'Resultado guardado')
        cy.get('html').should('not.contain', 'Traceback') 
    }),
    it('Agrega resultados experimento frustrado (fecha inválida)', function () {
        cy.login('jefe', '1a2s3d4f5g')
        cy.contains('Experimentos').click()
        cy.contains('Ver Experimentos').click()
        cy.get('.panel-body').click()
        cy.contains('Agregar resultados').click({
            force: true
        })
        cy.get('.table').find('select[name="proyecto"]').select('1', {force: true})
        cy.get('#datepicker-autoclose').click({force: true}).type('11/08/2015')
        cy.get('.table').find('select[name="protocolo"]').select('1', {force: true})
        cy.get('.table').find('input[name="resultados"]').click({force: true}).type('Resultados obtenidos con Cypress')
        cy.get('.table').find('input[type="radio"]').check('True', {force: true})
        cy.get('.table').find('input[name="observaciones"]').click({force: true}).type('Observaciones obtenidas con Cypress')
        cy.contains('Guardar ').click({
            force: true
        })
        cy.get('html').should('not.contain', 'Resultado guardado')
        cy.get('html').should('not.contain', 'Traceback')      
    }),
    it('Agrega resultados experimento frustrado 2 (Sin información)', function () {
        cy.login('jefe', '1a2s3d4f5g')
        cy.contains('Experimentos').click()
        cy.contains('Ver Experimentos').click()
        cy.get('.panel-body').click()
        cy.contains('Agregar resultados').click({
            force: true
        })
        cy.contains('Guardar ').click({
            force: true
        })
        cy.get('html').should('not.contain', 'Resultado guardado')
        cy.get('html').should('not.contain', 'Traceback')      
    }),
    it('Agrega resultados experimento frustrado 3 (híbrido)', function () {
        cy.login('jefe', '1a2s3d4f5g')
        cy.contains('Experimentos').click()
        cy.contains('Ver Experimentos').click()
        cy.get('.panel-body').click()
        cy.contains('Agregar resultados').click({
            force: true
        })
        cy.get('.table').find('select[name="proyecto"]').select('1', {force: true})
        cy.get('#datepicker-autoclose').click({force: true}).type('11/08/2018')
        cy.get('.table').find('select[name="protocolo"]').select('1', {force: true})
        cy.get('.table').find('input[type="radio"]').check('True', {force: true})
        cy.get('.table').find('input[name="observaciones"]').click({force: true}).type('Observaciones obtenidas con Cypress')
        cy.contains('Guardar ').click({
            force: true
        })
        cy.get('html').should('not.contain', 'Resultado guardado')
        cy.get('html').should('not.contain', 'Traceback')      
    }),
    it('Agregar mezcla resultados', function () {
        cy.login('jefe', '1a2s3d4f5g')
        cy.contains('Experimentos').click()
        cy.contains('Ver Experimentos').click()
        cy.get('.panel-body').click()
        cy.contains('Agregar resultados').click({
            force: true
        })
        cy.contains('Agregar mezcla').click({
            force: true
        })
        cy.get('.table').find('select[name="insumo"]').select('1', {force: true})
        cy.get('.table').find('input[name="descripcion"]').click({force: true}).type('Descripción obtenida con Cypress')        
        cy.get('.table').find('select[name="maquina"]').select('1', {force: true})
        cy.contains('Guardar').click({
            force: true
        })
        cy.get('html').should('contain', 'Resultado guardado')
        cy.get('html').should('not.contain', 'Traceback') 
    }),
    it('Agregar mezcla resultados 2', function () {
        cy.login('jefe', '1a2s3d4f5g')
        cy.contains('Experimentos').click()
        cy.contains('Ver Experimentos').click()
        cy.get('.panel-body').click()
        cy.contains('Agregar resultados').click({
            force: true
        })
        cy.contains('Agregar mezcla').click({
            force: true
        })
        cy.get('.table').find('select[name="insumo"]').select('2', {force: true})
        cy.get('.table').find('input[name="descripcion"]').click({force: true}).type('Descripción obtenida con Cypress')        
        cy.get('.table').find('select[name="maquina"]').select('2', {force: true})
        cy.contains('Guardar').click({
            force: true
        })
        cy.get('html').should('contain', 'Resultado guardado')
        cy.get('html').should('not.contain', 'Traceback') 
    })
})