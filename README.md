# MISO4208 Wiki

## Aplicaciones seleccionadas - Iteraci&oacute;n 1

### Web
#### Limesurvey
https://github.com/LimeSurvey/LimeSurvey

Aplicaci&oacute;n para crear y responder encuestas

### Android
#### Hometap
https://github.com/js-acevedo10/HomeTap-User-Android

Aplicaci&oacute;n para solicitar servicio de limpieza por parte de empleadas dom&eacute;sticas

### iOS
#### Hometap
https://github.com/d-soto11/HomeTap-User-iOS

Aplicaci&oacute;n para solicitar servicio de limpieza por parte de empleadas dom&eacute;sticas


## Reporte de pruebas 1
[Reporte de pruebas: iteración 1](https://gitlab.com/MISO-4208-JS-DS/testing/wikis/Reporte-de-pruebas:-Iteraci%C3%B3n-1)

## Reporte de pruebas cruzadas 1
[Reporte de pruebas cruzadas: iteración 1](https://gitlab.com/MISO-4208-JS-DS/testing/wikis/Reporte-de-pruebas-cruzadas:-Iteraci%C3%B3n-1)

## Reporte de pruebas 2
[Reporte de pruebas: iteración 2](https://gitlab.com/MISO-4208-JS-DS/testing/wikis/Reporte-de-pruebas:-Iteración-2)

## Reporte de pruebas cruzadas 2
[Reporte de pruebas cruzadas: iteración 2](https://gitlab.com/MISO-4208-JS-DS/testing/wikis/Reporte-de-pruebas-cruzadas:-iteracion-2)

## Reporte de pruebas 3
[Reporte de pruebas: iteración 3](https://docs.google.com/presentation/d/1o4QvMTIh9EIIcErlNSwQwEOXMMhLV79cIx3yWpadeKM/edit#slide=id.p)
